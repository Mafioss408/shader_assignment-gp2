Shader "GiuseppeNigro/Cristallo1"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        
        [Header(RimLight Setting)]
        [Space(10)]
        _RimColor("Rim Color",Color) = (1,1,1,1)
        _InnerColor("Inner Color", Color) = (1,1,1,1)
        _RimIntensity("Rim intensity", Range(0,1)) = 0
        _RimPower("Rim Power", Range(0,5)) = 1

        [Header(Distortion Setting)]
        [Space(10)]
        _DistortionPower("Distortion Power",Range(0,1)) = 0.1



    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }

        Blend One SrcColor
        //Blend SrcAlpha OneMinusSrcAlpha => test di tasparenza

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;

                float3 worldNormal : TEXCOORD1;
                float3 viewDirection : TEXCOORD2;
                float4 screenPos: TEXCOORD3;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            
            // variable RimLight
            float4 _RimColor;
            float4 _InnerColor;
            float _RimPower;
            float _RimIntensity;

            // variable Distortion
            float _DistortionPower;


            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.viewDirection = WorldSpaceViewDir(v.vertex);
                o.screenPos = ComputeScreenPos(v.vertex);
                return o;
            }


            float4 rimLight(float4 color, float3 normal, float3 viewDirection)
            {
                float rim = 1 - saturate(dot(normal, viewDirection));
                rim = pow(rim, _RimPower);
                rim *= _RimIntensity;
                float4 setColor = lerp(color, _RimColor, rim);
                return setColor;
            }


            fixed4 frag(v2f i) : SV_Target
            {
                i.worldNormal = normalize(i.worldNormal);
                i.viewDirection = normalize(i.viewDirection);
                
                fixed4 col = tex2D(_MainTex, i.uv);

               // Add InnerColor
                col.rgb *= _InnerColor.rgb * _InnerColor.a; 
                // Add RimLight
                col.rgb = rimLight(col, i.worldNormal, i.viewDirection);

                return col;
            }
            ENDCG
        }
    }
}
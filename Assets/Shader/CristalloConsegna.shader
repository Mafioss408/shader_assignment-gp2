Shader "GiuseppeNigro/Cristallo"
{
    Properties
    {
        [Header(RimLight Setting)]
        [Space(10)]
        _RimColor("Rim Color",Color) = (1,1,1,1)
        _InnerColor("Inner Color", Color) = (1,1,1,1)
        _RimIntensity("Rim intensity", Range(0,1)) = 0
        _RimPower("Rim Power", Range(0,5)) = 1

        [Header(Refraction Setting)]
        [Space(10)]
        _RefractPower ("Refraction Power", Range(0, 1)) = 0.1
        _RefractIndex ("Refraction Index", Range(1,1.5)) = 1.25
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent" "RenderType"="Transparent"
        }
        GrabPass
        {
            "_GrabTexture"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldNormal : TEXCOORD1;
                float3 viewDirection : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
            };


            // variable Distortion
            sampler2D _GrabTexture;
            float _RefractPower;
            float _RefractIndex;

            // variable RimLight
            float4 _RimColor;
            float4 _InnerColor;
            float _RimPower;
            float _RimIntensity;


            float4 rimLight(float4 color, float3 normal, float3 viewDirection)
            {
                float rim = 1 - saturate(dot(normal, viewDirection));
                rim = pow(rim, _RimPower) * _RimIntensity;
                return lerp(color, _RimColor, rim);
            }

            float2 reflection(float3 viewDirection, float3 normal, v2f i)
            {
                // Calcolo la refrazione
                float2 refractionOffset = refract(-viewDirection, normal, 1.0 / _RefractIndex).xy * _RefractPower;

                // Aggiungo la rifrazione all'uv
                float2 screenUV = i.screenPos.xy / i.screenPos.w;
                
                return screenUV + refractionOffset;
            }

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.viewDirection = normalize(WorldSpaceViewDir(v.vertex));
                o.screenPos = ComputeScreenPos(o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                
                float3 normal = normalize(i.worldNormal);
                float3 viewDir = normalize(i.viewDirection);
                
                // Aggiungo la rifrazione alla grabTexture
                fixed4 grabbedColor = tex2D(_GrabTexture, reflection(viewDir, normal, i));

                // Aggiungo Colore interno
                grabbedColor.rgb *= _InnerColor.rgb;

                // Aggiungo Colore RimLight
                fixed4 rim = rimLight(grabbedColor, normal, viewDir);
                
                return rim;
            }
            ENDCG
        }
    }
    Fallback "Transparent/Diffuse"
}